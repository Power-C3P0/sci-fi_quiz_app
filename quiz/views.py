from django.http import QueryDict
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from quiz.forms import NewQuestionform
from quiz.models import QuestModel




# Create your views here.


def newQuestion(request):    
    if request.user.is_staff:
        form=NewQuestionform()
        if(request.method=='POST'):
            form=NewQuestionform(request.POST)
            if(form.is_valid()):
                form.save()
                return redirect('/')
        context={'form':form}
        return render(request,'newquest.html',context)
    else: 
        return redirect('home') 

@login_required(redirect_field_name='login')    
def starwarsquiz(request):
        if request.method == 'POST':
            questions = QuestModel.objects.all()
            score=0
            wrong=0
            correct=0
            total=0
            for quest in questions:
                total += 1
                print(request.POST.get(quest.question))
                print(quest.ans)
                print()
                if quest.ans == request.POST.get(quest.question):
                    score+=10
                    correct+=1
                else:
                    wrong+=1
                
            percent = score/(total*10) *100
            context = {
                'score':score,
                'correct':correct,
                'wrong':wrong,
                'percent':percent,
                'total':total
            }
            return render(request,'result.html',context)
        else:
            questions = QuestModel.objects.all()
            context = {
                'questions': questions
            }
            return render(request,'quiz.html',context)        