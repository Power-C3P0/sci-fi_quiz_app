from django.contrib import admin
from quiz.models import QuestModel
# Register your models here.

class QuestModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(QuestModel, QuestModelAdmin)