from django.forms import ModelForm
from quiz.models import QuestModel


class NewQuestionform(ModelForm):
    class Meta:
        model=QuestModel
        fields="__all__"