from django.urls import path
from quiz.views import newQuestion, starwarsquiz
urlpatterns = [
    path("newquestion/", newQuestion, name="newquestion"),
    path("", starwarsquiz, name='starwarsquiz')
]
